if (UNIX)
    file(GLOB_RECURSE LibGLUT
    "/lib/libglut.so.*.*.*")
elseif(WIN32 AND MINGW)
    message(STATUS "WIN32 and MINGW system.")
    string(REGEX REPLACE "\\\\" "/" MINGW_PREFIX $ENV{MINGW_PREFIX})
    file(GLOB_RECURSE LibGLUT
    "${MINGW_PREFIX}/bin/libfreeglut.dll*")
endif()
message(STATUS "Lib name is ${LibGLUT}")