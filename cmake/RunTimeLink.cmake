add_library(Definitions INTERFACE)
if (UNIX)
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
    set(runtime_destination "lib/")
elseif(WIN32)
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/bin")
    set(runtime_destination "bin/")
    
    set(CMAKE_EXE_LINKER_FLAGS " -static")
    target_compile_definitions(Definitions INTERFACE
        GLUT_STATIC_LIB
    )
endif()