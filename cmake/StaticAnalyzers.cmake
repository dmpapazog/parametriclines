option(ENABLE_CPPCHECK "Enable static analysis with cppcheck" OFF)
option(ENABLE_CLANG_TIDY "Enable static analysis with clang-tidy" ON)
option(ENABLE_INCLUDE_WHAT_YOU_USE "Enable static analysis with include-what-you-use" OFF)

if(ENABLE_CPPCHECK)
  message(CHECK_START "Finding cppcheck")
  find_program(CPPCHECK cppcheck)
  if(CPPCHECK)
    set(CMAKE_CXX_CPPCHECK
        ${CPPCHECK}
        --suppress=missingInclude
        --enable=all
        --inline-suppr
        --inconclusive
        -i
        ${CMAKE_SOURCE_DIR}/imgui/lib)
    message(CHECK_PASS "found")
  else()
    message(SEND_ERROR "cppcheck requested but executable not found")
    message(CHECK_FAIL "not found")
  endif()
endif()

if(ENABLE_CLANG_TIDY)
  message(CHECK_START "Finding clang-tidy")
  find_program(CLANGTIDY clang-tidy)
  if(CLANGTIDY)
    set(CMAKE_CXX_CLANG_TIDY ${CLANGTIDY} -extra-arg=-Wno-unknown-warning-option)
    message(CHECK_PASS "found")
  else()
    message(SEND_ERROR "clang-tidy requested but executable not found")
    message(CHECK_FAIL "not found")
  endif()
endif()

if(ENABLE_INCLUDE_WHAT_YOU_USE)
  message(CHECK_START "Finding include-what-you-use")
  find_program(INCLUDE_WHAT_YOU_USE include-what-you-use)
  if(INCLUDE_WHAT_YOU_USE)
    set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE ${INCLUDE_WHAT_YOU_USE})
    message(CHECK_PASS "found")
  else()
    message(SEND_ERROR "include-what-you-use requested but executable not found")
    message(CHECK_FAIL "not found")
  endif()
endif()
