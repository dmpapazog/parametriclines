width  = 500,
height = 500,

x \in [0,  width] ακέραιος
y \in [0, height] ακέραιος

xp = x / width
yp = y / height

u \in [uMin., uMax.]
v \in [vMin., vMax.]

u = uMin + xp * (uMax - uMin)
v = vMin + yp * (vMax - vMin)
