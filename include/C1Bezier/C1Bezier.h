#ifndef C1BEZIER_H
#define C1BEZIER_H

#include "Utilities/Utilities.h"

extern void c1Bezier(int size);
extern void c1BezierAddPoint(float u, float v, int size);
extern void c1BezierHandler(int x, int y, int selection);
extern Point *getC1BezierControls();

#endif // C1BEZIER_H