#ifndef MYHEADER_H
#define MYHEADER_H

extern void display(void);
extern void mouse(int button, int state, int x, int y);
extern void reshape(int width, int height);
extern void motion(int x, int y);
extern void menu(int id);
extern void initLine();
// void idle(void);
// void keyboard(int key, int x, int y);

#endif // MYHEADER_H