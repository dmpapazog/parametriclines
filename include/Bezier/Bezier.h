#ifndef BEZIER_H
#define BEZIER_H

#include "Utilities/Utilities.h"

extern void bezier(int size);
extern void bezierAddPoint(float u, float v, int size);
extern void bezierHandler(int x, int y, int selection);
extern Point *getBezierControls();

#endif // BEZIER_H