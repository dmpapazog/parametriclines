#ifndef UTILITIES_H
#define UTILITIES_H

#include <GL/glut.h>

typedef GLfloat Point[3];

typedef struct Dim {
    int width;
    int height;
} Dim;

extern Dim viewPort;
extern float windowWidth;

extern void add(Point const lhs, Point const rhs, Point result);
extern void print(char const* name, Point const p);
extern void copyPoint(Point const source, Point target);
extern void convert(int x, int y, float *u, float *v);

#endif // UTILITIES_H