#ifndef SURFACE_H
#define SURFACE_H

#include "Utilities/Utilities.h"

extern void surface(int size);
extern void convertPoints();
extern void surfaceProjection();
void surfaceKeyboard(int key, int x, int y);
extern GLfloat const C[4][4];

#endif // SURFACE_H