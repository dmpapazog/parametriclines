#ifndef CUBIC_H
#define CUBIC_H

#include "Utilities/Utilities.h"

extern void cubic(int size);
extern void cubicAddPoint(float u, float v, int size);
extern void convertControlPoints();
extern Point *getCubicControls();
extern void cubicHandler(int x, int y, int selection);
extern GLfloat const C[4][4];

#endif // CUBIC_H