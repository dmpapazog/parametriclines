#include <GL/freeglut.h>
#include <stdio.h>

#include "Utilities/Utilities.h"
#include "Cubic/Cubic.h"

Point cubicP[7] = {
    {-0.5, -1.0, 0.0},
    {-1.0,  0.0, 0.0},
    {-0.5,  1.0, 0.0},
    { 0.0,  1.0, 0.0},
    { 0.5,  1.0, 0.0},
    { 1.0,  0.0, 0.0},
    { 0.5, -1.0, 0.0},
};

Point bezierP[7] = {
    {-0.5, -1.0, 0.0},
    {-1.0,  0.0, 0.0},
    {-0.5,  1.0, 0.0},
    { 0.0,  1.0, 0.0},
    { 0.5,  1.0, 0.0},
    { 1.0,  0.0, 0.0},
    { 0.5, -1.0, 0.0},
};

Point drawP[4] = {
    {-0.5, -1.0, 0.0},
    {-1.0,  0.0, 0.0},
    {-0.5,  1.0, 0.0},
    { 0.0,  1.0, 0.0}
};

GLfloat const C[4][4] = {
  {     1.,    0.,    0.,      0.},
  {-0.833f,    3.,  -1.5,  0.333f},
  { 0.333f,  -1.5,    3., -0.833f},
  {     0.,    0.,    0.,      1.}
};

void convertControlPoints();

void cubic(int size)
{
  if (size >= 0) {
    int i = 0; 
    glColor3f(1., 0., 0.);
    for (; i <= size && i < 3; ++i) {
      glBegin(GL_POINTS);
        glVertex3fv(cubicP[i]);
      glEnd();
    }

    if (size >= 3) {
      glColor3f(0., 1., 0.);
      glBegin(GL_POINTS);
        glVertex3fv(cubicP[i]);
      glEnd();

      glColor3f(0., 0., 1.);
      for (i += 1; i <= size; ++i) {
        glBegin(GL_POINTS);
          glVertex3fv(cubicP[i]);
        glEnd();
      }
      
      if (size == 6) {
        glColor3f(1., 1., 1.);

        convertControlPoints();
        for (int s = 0; s < 2; ++s) {
          for (int j = s * 3; j < s * 3 + 4; ++j) {
              copyPoint(bezierP[j], drawP[j - s * 3]);
          }

          glMap1f(GL_MAP1_VERTEX_3, 0., 1., 3, 4, *drawP);
          glBegin(GL_LINE_STRIP);
            for(i = 0; i < 100; i++)
              glEvalCoord1f((float)i / 100);
          glEnd();
        }
      }
    }
  }
}

void convertControlPoints()
{
  // q_0;
  // i = τα 4 bezier control points
  // j = οι 3 συνιστώσες
  // k = τα 4 cubic control points
  for (int s = 0; s < 2; ++s) {
    for (int i = s * 3; i < s *3 + 4; ++i) {
      for (int j = 0; j < 3; ++j) {
        bezierP[i][j] = 0.;
        for (int k = s * 3; k < s * 3 + 4; ++k) {
          bezierP[i][j] += C[i - s * 3][k - s * 3] * cubicP[k][j];
        }
      }
    }
  }
}

void cubicHandler(int x, int y, int selection)
{
  GLfloat u, v;
  convert(x, y, &u, &v);
  cubicP[selection][0] = u;
  cubicP[selection][1] = v;
  glutPostRedisplay();
}

Point *getCubicControls()
{
  return cubicP;
}

void cubicAddPoint(float u, float v, int size)
{
  cubicP[size][0] = u;
  cubicP[size][1] = v;
}