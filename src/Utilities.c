#include <stdio.h>
#include "Utilities/Utilities.h"

void add(Point const lhs, Point const rhs, Point result)
{
    result[0] = lhs[0] + rhs[0];
    result[1] = lhs[1] + rhs[1];
    result[2] = lhs[2] + rhs[2];
}

void copyPoint(Point const source, Point target)
{
    target[0] = source[0];
    target[1] = source[1];
    target[2] = source[2];
}

void print(char const *name, Point const p)
{
    if (name)
        printf("%s = [%f, %f, %f]\n", name, (double)p[0], (double)p[1], (double)p[2]);
    else
        printf("[%f, %f, %f]\n", (double)p[0], (double)p[1], (double)p[2]);
}

void convert(int x, int y, float *u, float *v)
{
  GLfloat xp = (GLfloat)x / (GLfloat)viewPort.width;
  GLfloat yp = (GLfloat)y / (GLfloat)viewPort.height;

  GLfloat step = windowWidth / 2;
  *u = -step + xp * windowWidth;
  *v =  step - yp * windowWidth;
}