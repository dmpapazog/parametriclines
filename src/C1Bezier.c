#include <GL/freeglut.h>
#include <stdio.h>

#include "Utilities/Utilities.h"
#include "C1Bezier/C1Bezier.h"

Point c1B[7] = {
  {-1.0 ,  0.0, 0.0},
  {-0.25,  1.0, 0.0},
  { 0.25,  1.0, 0.0},
  { 1.0 ,  0.0, 0.0},
  { 0.25, -1.0, 0.0},
  {-0.25, -1.0, 0.0},
  {-0.25, -1.0, 0.0}
};

Point drawC1[4] = {
    {-0.5, -1.0, 0.0},
    {-1.0,  0.0, 0.0},
    {-0.5,  1.0, 0.0},
    { 0.0,  1.0, 0.0}
};

void c1BezierHandler(int x, int y, int selection)
{
  GLfloat u, v;
  convert(x, y, &u, &v);
  if (selection != 3) {
    c1B[selection][0] = u;
    c1B[selection][1] = v;

    if (selection == 2) {
      c1B[4][0] = 2 * c1B[3][0] - c1B[2][0] ;
      c1B[4][1] = 2 * c1B[3][1] - c1B[2][1] ;
    }
    
    if (selection == 4) {
      c1B[2][0] = 2 * c1B[3][0] - c1B[4][0] ;
      c1B[2][1] = 2 * c1B[3][1] - c1B[4][1] ;
    }
  } else {
    Point diff;
    diff[0] = u - c1B[3][0];
    diff[1] = v - c1B[3][1];

    c1B[2][0] += diff[0];
    c1B[2][1] += diff[1];

    c1B[4][0] += diff[0];
    c1B[4][1] += diff[1];
    
    c1B[3][0] = u;
    c1B[3][1] = v;
  }
  glutPostRedisplay();
}

void c1BezierAddPoint(float u, float v, int size)
{
  if (size <= 3) {
    c1B[size][0] = u;
    c1B[size][1] = v;
  } else {
    c1B[size + 1][0] = u;
    c1B[size + 1][1] = v;
  }
  if (size == 3) {
    c1B[4][0] = 2 * c1B[3][0] - c1B[2][0] ;
    c1B[4][1] = 2 * c1B[3][1] - c1B[2][1] ;
  }
}

void c1Bezier(int size)
{
  if (size >= 0) {
    glColor3f(1., 0., 0.);
    int i = 0;
    for (; i <= size && i < 2; ++i) {
      glBegin(GL_POINTS);
        glVertex3fv(c1B[i]);
      glEnd();
    }

    if (size >= 2) {
      glBegin(GL_POINTS);
        glColor3f(1., 0.65f, 0.);
        glVertex3fv(c1B[i++]); // 3
      glEnd();

      if (size >= 3) {
        glBegin(GL_POINTS);
          glColor3f(0., 1., 0.);
          glVertex3fv(c1B[i++]); // 4
        glEnd();
        
        glBegin(GL_POINTS);
          glColor3f(0., 1., 1.);
          glVertex3fv(c1B[i++]); // 5
        glEnd();

        glColor3f(0., 0., 1.);
        for (; i <= size + 1; ++i) {
          glBegin(GL_POINTS);
            glVertex3fv(c1B[i]);
          glEnd();
        }

        if (size == 5) {
          glColor3f(1., 1., 1.);
          for (int s = 0; s < 2; ++s) {
            for (int j = s * 3; j < s * 3 + 4; ++j) {
              copyPoint(c1B[j], drawC1[j - s * 3]);
            }

            glMap1f(GL_MAP1_VERTEX_3, 0., 1., 3, 4, *drawC1);
            glBegin(GL_LINE_STRIP);
              for(i = 0; i < 100; i++)
                glEvalCoord1f((float)i / 100);
            glEnd();
          }
        }
      }
    }
  }
}

Point *getC1BezierControls()
{
  return c1B;
}