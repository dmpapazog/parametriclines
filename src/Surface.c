#include "Surface/Surface.h"


Point const cubicPoints[16] = {
    { 0.0, -1.0 , -1.0 }, //1
    { 1.0, -0.25, -1.0 },
    {-1.0,  0.25, -1.0 },
    { 0.0,  1.0 , -1.0 }, //2
    { 1.0, -1.0 , -0.25}, //
    {-1.0, -0.25, -0.25},
    { 0.0,  0.25, -0.25},
    { 1.0,  1.0 , -0.25}, //
    {-1.0, -1.0 ,  0.25}, //
    { 0.0, -0.25,  0.25},
    { 1.0,  0.25,  0.25},
    {-1.0,  1.0 ,  0.25}, //
    { 0.0, -1.0 ,  1.0 }, //3
    { 1.0, -0.25,  1.0 },
    {-1.0,  0.25,  1.0 },
    { 0.0,  1.0 ,  1.0 }  //4
};

Point bezierPoints[16] = {
    {0.0, 0.0, 0.0}, //1
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0}, //2
    {0.0, 0.0, 0.0}, //
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0}, //
    {0.0, 0.0, 0.0}, //
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0}, //
    {0.0, 0.0, 0.0}, //3
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0}  //4
};

GLfloat angleX = 0.0;
GLfloat angleZ = 0.0;
GLfloat const step = 5.0;

void zeroBezierPoints();

void surface(int size)
{
  glColor3f(1., 0., 0.);
  glBegin(GL_POINTS);
    for (int i = 0; i < 16; ++i) {
      if (i != 0 && i != 3 && i != 12 && i != 15)
        glVertex3fv(cubicPoints[i]);
    }
  glEnd();
  glColor3f(0., 1., 0.);
  glBegin(GL_POINTS);
    glVertex3fv(cubicPoints[0]);
    glVertex3fv(cubicPoints[3]);
    glVertex3fv(cubicPoints[12]);
    glVertex3fv(cubicPoints[15]);
  glEnd();

  convertPoints();
  glColor3f(1., 1., 1.);
  glMap2f(GL_MAP2_VERTEX_3, 0., 1., 3, 4, 0., 1., 12, 4, *bezierPoints);
  for(int j = 0; j < 100; ++j) {
    glBegin(GL_LINE_STRIP);
      for(int i = 0; i < 100; i++)
        glEvalCoord2f((float)i / 100, (float)j / 100);
    glEnd();
    glBegin(GL_LINE_STRIP);
      for(int i = 0; i < 100; i++)
        glEvalCoord2f((float)j / 100, (float)i / 100);
    glEnd();
  }
}

void convertPoints()
{
  // q_0;
  // s = οι 4 καμπύλες
  // b = τα 4 bezier control points
  // i = οι 3 συνιστώσες
  // c = τα 4 cubic control points
  zeroBezierPoints();

  for (int comp = 0; comp < 3; ++comp) {
    GLfloat aux[4][4] = {
      {0, 0, 0, 0},
      {0, 0, 0, 0},
      {0, 0, 0, 0},
      {0, 0, 0, 0}
    };
    // C @ P[comp]
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) {
        for (int k = 0; k < 4; ++k) {
          aux[i][j] += C[i][k] * cubicPoints[k + j * 4][comp];
        }
      }
    }

    // aux @ C.T
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) {
        for (int k = 0; k < 4; ++k) {
          bezierPoints[i + j * 4][comp] += aux[i][k] * C[j][k];
        }
      }
    }
  }
}

void zeroBezierPoints()
{
  for (int p = 0; p < 16; ++p) {
    bezierPoints[p][0] = bezierPoints[p][1] = bezierPoints[p][2] = 0.;
  }
}

void surfaceProjection()
{
  glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // glFrustum(-20, 20, -20, 20, 0, 10);  
    glOrtho(-2.,
             2.,
            -2.,
             2.,
             0., 10.);
  glMatrixMode(GL_MODELVIEW);
  
  glLoadIdentity();
  gluLookAt(0.3, 0, -2, 0., 0., 0., 0.98, 0, 0.14);
}

void surfaceKeyboard(int key, int x, int y)
{
  if (key == GLUT_KEY_RIGHT) {
     angleX += step;
     if (angleX >= 360) {
       angleX = 0;
     }
    // glLoadIdentity();
    // gluLookAt(0.3, 0, 0.5, 0., 0.5, 0.5, 0.447, 0.89, 0.);
    glRotatef(angleZ, 0., 1., 0.);
    glRotatef(step, 1., 0., 0.);
    glRotatef(-angleZ, 0., 1., 0.);
  }
  if (key == GLUT_KEY_LEFT) {
     angleX -= step;
     if (angleX <= 0) {
       angleX = 360;
     }
    // glLoadIdentity();
    // gluLookAt(0.3, 0, 0.5, 0., 0.5, 0.5, 0.447, 0.89, 0.);
    glRotatef(angleZ, 0., 1., 0.);
    glRotatef(-step, 1., 0., 0.);
    glRotatef(-angleZ, 0., 1., 0.);
  }
  if (key == GLUT_KEY_UP) {
     angleZ += step;
     if (angleZ >=360) {
       angleZ = 0;
     }
    // glLoadIdentity();
    // gluLookAt(0.3, 0, 0.5, 0., 0.5, 0.5, 0.447, 0.89, 0.);
    glRotatef(angleX, 1., 0., 0.);
    glRotatef(step, 0., 1., 0.);
    glRotatef(-angleX, 1., 0., 0.);
  }
  if (key == GLUT_KEY_DOWN) {
     angleZ -= step;
     if (angleZ <= 0) {
       angleZ = 360;
     }
    // glLoadIdentity();
    // gluLookAt(0.3, 0, 0.5, 0., 0.5, 0.5, 0.447, 0.89, 0.);
    glRotatef(angleX, 1., 0., 0.);
    glRotatef(-step, 0., 1., 0.);
    glRotatef(-angleX, 1., 0., 0.);
  }
  glutPostRedisplay();
}