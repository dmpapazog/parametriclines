#include <GL/glut.h>
#include <stdio.h>
#include "MyHeader/MyHeader.h"
#include "Utilities/Utilities.h"


void init(void);
void initWindow(void);
void initMenu(void);
void setCallbacks(void);


Dim viewPort;
float windowWidth;

int main(int argc, char **argv) {
  viewPort.width  = 500;
  viewPort.height = 500;
  windowWidth = 4.;
  
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  initWindow();
  init();
  setCallbacks();
  initMenu();

  glutMainLoop();
  return 0;
}

void init(void)
{
  glClearColor(0, 0, 0, 0);
  glPointSize(10.0);
  // glShadeModel(GL_SMOOTH);
  // glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  // glEnable(GL_LIGHTING);
  // glEnable(GL_LIGHT0);
  glEnable(GL_DEPTH_TEST);

  glEnable(GL_MAP1_VERTEX_3);
  glEnable(GL_MAP2_VERTEX_3);

  glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-windowWidth / 2,
             windowWidth / 2,
            -windowWidth / 2,
             windowWidth / 2,
             0., 2.);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  // gluLookAt(0., 1., 1., 0., 0., 0., .0, 1., -0.1);
  initLine();
}

void initWindow(void)
{
  glutInitWindowSize(viewPort.width, viewPort.height);
  glutInitWindowPosition(100, 50);
  glutCreateWindow("Testing");
}

void setCallbacks(void)
{
  glutDisplayFunc(&display);
  glutMouseFunc(&mouse);
  glutReshapeFunc(&reshape);
  // glutIdleFunc(&idle);
  // glutSpecialFunc(&keyboard);
}

void initMenu(void)
{
  glutCreateMenu(&menu);
    glutAddMenuEntry("Bezier.", 1);
    glutAddMenuEntry("Cubic", 2);
    glutAddMenuEntry("C1Bezier", 3);
    glutAddMenuEntry("Surface", 4);
  glutAttachMenu(GLUT_RIGHT_BUTTON);
}