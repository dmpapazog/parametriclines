#include <GL/freeglut.h>
#include <stdio.h>
#include <math.h>

#include "MyHeader/MyHeader.h"
#include "Utilities/Utilities.h"
#include "Bezier/Bezier.h"
#include "Cubic/Cubic.h"
#include "C1Bezier/C1Bezier.h"
#include "Surface/Surface.h"

GLfloat const error = 0.2f;
GLint selection = -1;

GLint closest(float u, float v);
Point *controls = NULL;
GLint size = 0;
GLint maxSize = 0;

GLboolean reset = GL_FALSE;
void resetProjection();

void (*motionHandler)(int x, int y, int selection);
void (*drawLine)(int size);
void (*addPoint)(float u, float v, int size);

void rectangle(void)
{
  Point points[4] = {
    {-1.0, -1.0, 0.0},
    {-1.0,  1.0, 0.0},
    { 1.0,  1.0, 0.0},
    { 1.0, -1.0, 0.0}
  };

  glColor3f(0.2f, 1., 1.);
  glBegin(GL_QUADS);
    glVertex3fv(points[0]);
    glVertex3fv(points[1]);
    glVertex3fv(points[2]);
    glVertex3fv(points[3]);
  glEnd();
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  drawLine(size);
  // rectangle();
  // convertControlPoints();
  // cubic();

  glutSwapBuffers();
}

void mouse(int button, int state, int x, int y)
{
  if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
    GLfloat u, v;
    convert(x, y, &u, &v);
    printf("x = %d, y %d, u = %.3f, v = %.3f\n", x, y, (double)u, (double)v);

    if (size < maxSize) {

      addPoint(u, v, ++size);
      glutPostRedisplay();
    } else {
      selection = closest(u, v);
      if (selection >= 0) {
        glutMotionFunc(&motion);
        if (controls)
          print(NULL, controls[selection]);
      }
    }
  }
  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    glutMotionFunc(NULL);
    selection = -1;
  }
}

void motion(int x, int y)
{
  if (motionHandler)
    motionHandler(x, y, selection);
}

void reshape(int width, int height)
{
  viewPort.width  = width;
  viewPort.height = height;

  glViewport(0, 0, width, height);
  // glMatrixMode(GL_PROJECTION);
  //   glLoadIdentity();
  //   glOrtho(-windowWidth / 2,
  //            windowWidth / 2,
  //           -windowWidth / 2,
  //            windowWidth / 2,
  //            0., 2.);
  // glMatrixMode(GL_MODELVIEW);
  glutPostRedisplay();
}

int closest(float u, float v)
{
  if (controls) {
    for (int i = 0; i < size + 2; ++i) {
      if (fabsf(u - controls[i][0]) < error && fabsf(v - controls[i][1]) < error)
        return i;
    }
  }
  return -1;
}

void menu(int id)
{
  switch (id)
  {
  case 1:
    resetProjection();
    controls = getBezierControls();
    motionHandler = &bezierHandler;
    drawLine = &bezier;
    addPoint = &bezierAddPoint;
    size = -1;
    maxSize = 5;
    break;
  case 2:
    resetProjection();
    controls = getCubicControls();
    motionHandler = &cubicHandler;
    drawLine = &cubic;
    addPoint = &cubicAddPoint;
    size = -1;
    maxSize = 6;
    break;
  case 3:
    resetProjection();
    controls = getC1BezierControls();
    motionHandler = &c1BezierHandler;
    drawLine = &c1Bezier;
    addPoint = &c1BezierAddPoint;
    size = -1;
    maxSize = 5;
    break;
  case 4:
    controls = NULL;
    motionHandler = NULL;
    drawLine = &surface;
    addPoint = NULL;
    size = 16;
    maxSize = 16;
    surfaceProjection();
    reset = GL_TRUE;
    glutSpecialFunc(&surfaceKeyboard);
    break;
  default:
    break;
  }
  glutPostRedisplay();
}

void initLine()
{
  controls = getBezierControls();
  motionHandler = &bezierHandler;
  drawLine = &bezier;
  addPoint = &bezierAddPoint;
  size = -1;
  maxSize = 5;
}

void resetProjection()
{
  if (reset) {
    glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      glOrtho(-windowWidth / 2,
               windowWidth / 2,
              -windowWidth / 2,
               windowWidth / 2,
               0., 2.);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    reset = GL_FALSE;
    glutKeyboardFunc(NULL);
  }
}