#include <GL/freeglut.h>
#include <stdio.h>

#include "Utilities/Utilities.h"
#include "Bezier/Bezier.h"

Point controlsB[7] = {
    {-1.0 ,  0.0, 0.0},
    {-0.25,  1.0, 0.0},
    { 0.25,  1.0, 0.0},
    { 1.0 ,  0.0, 0.0},
    { 0.25, -1.0, 0.0},
    {-0.25, -1.0, 0.0},
    {-1.0 ,  0.0, 0.0},
};

void bezier(int size)
{
  if (size >= 0) {
    glColor3f(0., 1., 0.);
    glBegin(GL_POINTS);
      glVertex3fv(controlsB[0]);
    glEnd();
  }

  glColor3f(1., 0., 0.);
  for (int i = 1; i <= size; ++i) {
    glBegin(GL_POINTS);
      glVertex3fv(controlsB[i]);
    glEnd();
  }
  
  if (size == 5) {
    glColor3f(1., 1., 1.);
    glMap1f(GL_MAP1_VERTEX_3, 0., 1., 3, 7, *controlsB);
    glBegin(GL_LINE_STRIP);
      for(int i = 0; i < 100; i++)
        glEvalCoord1f((float)i / 100);
    glEnd();
  }
}

void bezierHandler(int x, int y, int selection)
{
  GLfloat u, v;
  convert(x, y, &u, &v);
  controlsB[selection][0] = u;
  controlsB[selection][1] = v;
  if (selection == 0) {
    controlsB[6][0] = u;
    controlsB[6][1] = v;
  }
  glutPostRedisplay();
}

Point *getBezierControls()
{
  return controlsB;
}

void bezierAddPoint(float u, float v, int size)
{
  controlsB[size][0] = u;
  controlsB[size][1] = v;
  if (size == 0) {
    controlsB[6][0] = u;
    controlsB[6][1] = v;
  }
}